﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using TourDuLich.Models;

namespace TourDuLich.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<DatTour> DatTours { get; set; }
        public DbSet<ChiTietDatTour> ChiTietDatTours { get; set; }
        public DbSet<HoaDon> HoaDons { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<LoaiTour> LoaiTours { get; set; }
        public DbSet<ThanhToanThe> ThanhToanThes { get; set; }

        public DbSet<KhuVuc> KhuVucs { get; set; }
        public DbSet<KhachHang> KhachHangs { get; set; }
        public DbSet<KhachSan> KhachSans { get; set; }
        public DbSet<DichVu> DichVus { get; set; }
    }
}
