﻿using Microsoft.AspNetCore.Mvc;
using TourDuLich.Data;

namespace TourDuLich.Controllers
{
    [ViewComponent(Name ="_Category")]
    public class _CategoryViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _context;

        public _CategoryViewComponent(ApplicationDbContext context)
        {
            _context = context;
        }

        public IViewComponentResult Invoke()
        {
            var _category = _context.LoaiTours.ToList();
            return View("_Category", _category);
        }
    }
}
