﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using TourDuLich.Data;
using TourDuLich.Models;
using System.Globalization;
using System.Text;
namespace TourDuLich.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Home( )
        {

            return View(_context.Tours.ToList());
        }

        public IActionResult About()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Search(string keywords)
        {
            // Kiểm tra xem từ khóa tìm kiếm có tồn tại không
            if (string.IsNullOrEmpty(keywords))
            {
                // Trả về tất cả các tour nếu không có từ khóa tìm kiếm
                return View("Home", await _context.Tours.ToListAsync());
            }

            // Chuyển đổi từ khóa tìm kiếm thành chữ thường và loại bỏ dấu
            var normalizedKeywords = RemoveAccents(keywords.ToLower());

            // Lấy danh sách tất cả các tour từ cơ sở dữ liệu
            var tours = await _context.Tours.ToListAsync();

            // Lọc các tour theo từ khóa tìm kiếm (không phân biệt dấu và không phân biệt hoa thường)
            var searchResults = tours.Where(t => RemoveAccents(t.Name.ToLower()).Contains(normalizedKeywords));

            return View("Home", searchResults);
        }

        // Phương thức để loại bỏ dấu từ chuỗi
        public string RemoveAccents(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormKD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormKC);
        }

        public async Task<IActionResult> TourByCategory(int catId)
        {
            var applicationDbContext = _context.Tours.Include(t => t.LoaiTour).Where(p => p.LoaiTourId == catId);
            return View(await applicationDbContext.ToListAsync());
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
