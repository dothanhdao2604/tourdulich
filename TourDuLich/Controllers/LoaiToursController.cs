﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TourDuLich.Data;
using TourDuLich.Models;

namespace TourDuLich.Controllers
{
    public class LoaiToursController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LoaiToursController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LoaiTours
        public async Task<IActionResult> Index()
        {
            return View(await _context.LoaiTours.ToListAsync());
        }

        // GET: LoaiTours/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiTour = await _context.LoaiTours
                .FirstOrDefaultAsync(m => m.Id == id);
            if (loaiTour == null)
            {
                return NotFound();
            }

            return View(loaiTour);
        }

        // GET: LoaiTours/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LoaiTours/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] LoaiTour loaiTour)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loaiTour);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(loaiTour);
        }

        // GET: LoaiTours/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiTour = await _context.LoaiTours.FindAsync(id);
            if (loaiTour == null)
            {
                return NotFound();
            }
            return View(loaiTour);
        }

        // POST: LoaiTours/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] LoaiTour loaiTour)
        {
            if (id != loaiTour.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loaiTour);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoaiTourExists(loaiTour.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(loaiTour);
        }

        // GET: LoaiTours/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loaiTour = await _context.LoaiTours
                .FirstOrDefaultAsync(m => m.Id == id);
            if (loaiTour == null)
            {
                return NotFound();
            }

            return View(loaiTour);
        }

        // POST: LoaiTours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loaiTour = await _context.LoaiTours.FindAsync(id);
            if (loaiTour != null)
            {
                _context.LoaiTours.Remove(loaiTour);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LoaiTourExists(int id)
        {
            return _context.LoaiTours.Any(e => e.Id == id);
        }
    }
}
