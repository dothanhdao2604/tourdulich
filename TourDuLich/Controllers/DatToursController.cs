﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TourDuLich.Data;
using TourDuLich.Models;

namespace TourDuLich.Controllers
{
    public class DatToursController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DatToursController(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> AddToCartAsync(int tourId)
        {
            var applicationDbContext = _context.DatTours.Include(d => d.KhachHang).Include(d => d.Tour);

            return View(await applicationDbContext.ToListAsync());
        }

        // GET: DatTours
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.DatTours.Include(d => d.KhachHang).Include(d => d.Tour);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: DatTours/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var datTour = await _context.DatTours
                .Include(d => d.KhachHang)
                .Include(d => d.Tour)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (datTour == null)
            {
                return NotFound();
            }

            return View(datTour);
        }

        // GET: DatTours/Create
        public IActionResult Create()
        {
            ViewData["KhachHangId"] = new SelectList(_context.KhachHangs, "Id", "Id");
            ViewData["TourId"] = new SelectList(_context.Tours, "Id", "Id");
            return View();
        }

        // POST: DatTours/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,KhachHangId,TourId,NgayDat,NgayDi,NgayVe,SoNguoi,TongGia")] DatTour datTour)
        {
            if (ModelState.IsValid)
            {
                _context.Add(datTour);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["KhachHangId"] = new SelectList(_context.KhachHangs, "Id", "Id", datTour.KhachHangId);
            ViewData["TourId"] = new SelectList(_context.Tours, "Id", "Id", datTour.TourId);
            return View(datTour);
        }

        // GET: DatTours/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var datTour = await _context.DatTours.FindAsync(id);
            if (datTour == null)
            {
                return NotFound();
            }
            ViewData["KhachHangId"] = new SelectList(_context.KhachHangs, "Id", "Id", datTour.KhachHangId);
            ViewData["TourId"] = new SelectList(_context.Tours, "Id", "Id", datTour.TourId);
            return View(datTour);
        }

        // POST: DatTours/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,KhachHangId,TourId,NgayDat,NgayDi,NgayVe,SoNguoi,TongGia")] DatTour datTour)
        {
            if (id != datTour.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(datTour);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DatTourExists(datTour.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["KhachHangId"] = new SelectList(_context.KhachHangs, "Id", "Id", datTour.KhachHangId);
            ViewData["TourId"] = new SelectList(_context.Tours, "Id", "Id", datTour.TourId);
            return View(datTour);
        }

        // GET: DatTours/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var datTour = await _context.DatTours
                .Include(d => d.KhachHang)
                .Include(d => d.Tour)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (datTour == null)
            {
                return NotFound();
            }

            return View(datTour);
        }

        // POST: DatTours/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var datTour = await _context.DatTours.FindAsync(id);
            if (datTour != null)
            {
                _context.DatTours.Remove(datTour);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DatTourExists(int id)
        {
            return _context.DatTours.Any(e => e.Id == id);
        }
    }
}
