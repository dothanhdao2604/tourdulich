﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TourDuLich.Migrations
{
    /// <inheritdoc />
    public partial class _3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HoaDons_ThanhToanThes_ThanhToanTheId1",
                table: "HoaDons");

            migrationBuilder.DropIndex(
                name: "IX_HoaDons_ThanhToanTheId1",
                table: "HoaDons");

            migrationBuilder.DropColumn(
                name: "ThanhToanTheId",
                table: "HoaDons");

            migrationBuilder.DropColumn(
                name: "ThanhToanTheId1",
                table: "HoaDons");

            migrationBuilder.CreateIndex(
                name: "IX_ThanhToanThes_HoaDonId",
                table: "ThanhToanThes",
                column: "HoaDonId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ThanhToanThes_HoaDons_HoaDonId",
                table: "ThanhToanThes",
                column: "HoaDonId",
                principalTable: "HoaDons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ThanhToanThes_HoaDons_HoaDonId",
                table: "ThanhToanThes");

            migrationBuilder.DropIndex(
                name: "IX_ThanhToanThes_HoaDonId",
                table: "ThanhToanThes");

            migrationBuilder.AddColumn<int>(
                name: "ThanhToanTheId",
                table: "HoaDons",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ThanhToanTheId1",
                table: "HoaDons",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HoaDons_ThanhToanTheId1",
                table: "HoaDons",
                column: "ThanhToanTheId1");

            migrationBuilder.AddForeignKey(
                name: "FK_HoaDons_ThanhToanThes_ThanhToanTheId1",
                table: "HoaDons",
                column: "ThanhToanTheId1",
                principalTable: "ThanhToanThes",
                principalColumn: "Id");
        }
    }
}
