﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TourDuLich.Migrations
{
    /// <inheritdoc />
    public partial class _42 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ThanhToanTheId1",
                table: "HoaDons",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HoaDons_ThanhToanTheId1",
                table: "HoaDons",
                column: "ThanhToanTheId1");

            migrationBuilder.AddForeignKey(
                name: "FK_HoaDons_ThanhToanThes_ThanhToanTheId1",
                table: "HoaDons",
                column: "ThanhToanTheId1",
                principalTable: "ThanhToanThes",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HoaDons_ThanhToanThes_ThanhToanTheId1",
                table: "HoaDons");

            migrationBuilder.DropIndex(
                name: "IX_HoaDons_ThanhToanTheId1",
                table: "HoaDons");

            migrationBuilder.DropColumn(
                name: "ThanhToanTheId1",
                table: "HoaDons");
        }
    }
}
