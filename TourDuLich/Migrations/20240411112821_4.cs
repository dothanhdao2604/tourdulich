﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TourDuLich.Migrations
{
    /// <inheritdoc />
    public partial class _4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ThanhToanThes_HoaDons_HoaDonId",
                table: "ThanhToanThes");

            migrationBuilder.DropIndex(
                name: "IX_ThanhToanThes_HoaDonId",
                table: "ThanhToanThes");

            migrationBuilder.AddColumn<int>(
                name: "ThanhToanTheId",
                table: "HoaDons",
                type: "int",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ThanhToanTheId",
                table: "HoaDons");

            migrationBuilder.CreateIndex(
                name: "IX_ThanhToanThes_HoaDonId",
                table: "ThanhToanThes",
                column: "HoaDonId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ThanhToanThes_HoaDons_HoaDonId",
                table: "ThanhToanThes",
                column: "HoaDonId",
                principalTable: "HoaDons",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
