﻿namespace TourDuLich.Models
{
    public class Image
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TourId { get; set; }
        public Tour? Tour { get; set; }
    }
}
