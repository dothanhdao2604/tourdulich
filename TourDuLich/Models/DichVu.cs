﻿using Microsoft.AspNetCore.Identity;

namespace TourDuLich.Models
{
    public class DichVu
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MoTaDichVu { get; set; }
        public decimal Gia { get; set; }

    }
}
