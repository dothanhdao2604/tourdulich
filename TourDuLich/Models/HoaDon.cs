﻿namespace TourDuLich.Models
{
    public class HoaDon
    {
        public int Id { get; set; }
        public int ChiTietDatTourId { get; set; }
        public DateTime NgayLap { get; set; }
        public decimal TongTien { get; set; }
        public string HinhThucThanhToan { get; set; }
        public string TrangThaiThanhToan { get; set; }
        public ThanhToanThe? ThanhToanThe { get; set; }
        public ChiTietDatTour? ChiTietDatTour { get; set; }
    }

}
