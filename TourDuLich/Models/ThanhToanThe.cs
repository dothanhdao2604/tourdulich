﻿using System.ComponentModel.DataAnnotations;

namespace TourDuLich.Models
{
    public class ThanhToanThe
    {
        public int Id { get; set; }
        public int HoaDonId { get; set; }
    
        public string SoThe { get; set; }
    
        public string TenChuThe { get; set; }
 
        public string NgayHetHan { get; set; }
       
        public string CVV { get; set; }
    }

}
