﻿using System.ComponentModel.DataAnnotations;

namespace TourDuLich.Models
{
    public class Tour
    {
        public int Id { get; set; }
        public string Name { get; set; }
    
        public string MoTa { get; set; }
        public decimal GiaTour { get; set; }
        public DateTime NgayBatDau { get; set; }
        public DateTime NgayKetThuc { get; set; }
        public string DiemKhoiHanh { get; set; }
        public string DiemDen { get; set; }
        public string?  ImageUrl { get; set; }
        public List<Image>? Images { get; set; }
        public int LoaiTourId { get; set; }
        public LoaiTour? LoaiTour { get; set; }
        
    }

}
