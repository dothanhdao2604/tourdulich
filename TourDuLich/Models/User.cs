﻿namespace TourDuLich.Models
{
    public class User
    {
        public int UsertId { get; set; }
        public string UserName { get; set; }
        public string UsertPassword { get; set; }
        public string UserEmail { get; set; }

    }
}
