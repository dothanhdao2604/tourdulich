﻿namespace TourDuLich.Models
{
    public class KhachSan
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string DiaChi { get; set; }
        public string SoDienThoai { get; set; }
        public string MoTa { get; set; }
        public decimal GiaPhongTrungBinh { get; set; }
        public string? ImageUrl { get; set; }
        public List<Image>? Images { get; set; }
        public int KhuVucId { get; set; }

        public KhuVuc? KhuVuc { get; set; }
    }
}
