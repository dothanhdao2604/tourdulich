﻿using System.ComponentModel.DataAnnotations;

namespace TourDuLich.Models
{
    public class DatTour
    {
        public int Id { get; set; }
        public int KhachHangId { get; set; }
        public int TourId { get; set; }
        public DateTime NgayDat { get; set; }
        public DateTime NgayDi { get; set; }
        public DateTime NgayVe { get; set; }
        public int SoNguoi { get; set; }
        public decimal TongGia { get; set; }

        public KhachHang? KhachHang { get; set; }
        public Tour? Tour { get; set; }
    }

}
