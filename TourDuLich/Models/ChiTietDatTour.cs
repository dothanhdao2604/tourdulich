﻿using System.ComponentModel.DataAnnotations;

namespace TourDuLich.Models
{
    public class ChiTietDatTour
    {
        public int Id { get; set; }
        public int DatTourId { get; set; }
        public int? DichVuId { get; set; }
        public int? KhachSanId { get; set; }
        public int SoLuong { get; set; }
        public decimal DonGia { get; set; }
        public decimal ThanhTien { get; set; }

        public DatTour? DatTour { get; set; }
        public DichVu? DichVu { get; set; }
        public KhachSan? KhachSan { get; set; }
    }

}
