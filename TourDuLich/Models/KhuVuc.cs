﻿namespace TourDuLich.Models
{
    public class KhuVuc
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string MoTaKhuVuc { get; set; }
        public string DacDiemNoiBat { get; set; }

    }
}
