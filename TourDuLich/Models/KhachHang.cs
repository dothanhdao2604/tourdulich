﻿
namespace TourDuLich.Models
{
    public class KhachHang 
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SoDienThoai { get; set; }
        public string DiaChi { get; set; }
       
        public string? ImageUrl { get; set; }
        public List<Image>? Images { get; set; }
    }
}
